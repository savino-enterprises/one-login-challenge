import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class FractionTest {
    @Test
    public void wholeNumberAddition() {
        String result = Fraction.calculate("5 + 8 + 34");
        assertEquals("47", result, String.format("Result [%s] must equal 47", result));
    }

    @Test
    public void wholeNumberSubtraction() {
        String result = Fraction.calculate("-5 - 8 - 34");
        assertEquals("-47", result, String.format("Result [%s] must equal -47", result));
    }

    @Test
    public void wholeNumberMultiplication() {
        String result = Fraction.calculate("5 * -8 * 34");
        assertEquals("-1360", result, String.format("Result [%s] must equal -1360", result));
    }

    @Test
    public void wholeNumberDivision() {
        String result = Fraction.calculate("5 / 8 / -34");
        assertEquals("-5/272", result, String.format("Result [%s] must equal -5/272", result));
    }

    @Test
    public void fractionAddition() {
        String result = Fraction.calculate("5/6 + 8/9 + 34/3");
        assertEquals("13_1/18", result, String.format("Result [%s] must equal 13_1/18", result));
    }

    @Test
    public void fractionSubtraction() {
        String result = Fraction.calculate("-5/6 - 8/9 - 34/3");
        assertEquals("-13_1/18", result, String.format("Result [%s] must equal -13_1/18", result));
    }

    @Test
    public void fractionMultiplication() {
        String result = Fraction.calculate("5/6 * 8/9 * -34/3");
        assertEquals("-8_32/81", result, String.format("Result [%s] must equal -8_32/81", result));
    }

    @Test
    public void fractionDivision() {
        String result = Fraction.calculate("5/6 / -8/9 / 34/3");
        assertEquals("-45/544", result, String.format("Result [%s] must equal -45/544", result));
    }

    @Test
    public void mixedNumberAddition() {
        String result = Fraction.calculate("2_5/6 + 1_8/9 + 3_4/3");
        assertEquals("9_1/18", result, String.format("Result [%s] must equal 9_1/18", result));
    }

    @Test
    public void mixedNumberSubtraction() {
        String result = Fraction.calculate("-2_5/6 - 1_8/9 - 3_4/3");
        assertEquals("-9_1/18", result, String.format("Result [%s] must equal -9_1/18", result));
    }

    @Test
    public void mixedNumberMultiplication() {
        String result = Fraction.calculate("-2_5/6 * 1_8/9 * 3_4/3");
        assertEquals("-23_31/162", result, String.format("Result [%s] must equal -23_31/162", result));
    }

    @Test
    public void mixedNumberDivision() {
        String result = Fraction.calculate("-2_5/6 / 1_8/9 / -3_4/3");
        assertEquals("9/26", result, String.format("Result [%s] must equal 9/26", result));
    }

    @Test
    public void orderOfOperations() {
        String result = Fraction.calculate("1/2 + -3/4 * 1_1/8 - -5/4 / 6_1/2");
        assertEquals("-63/416", result, String.format("Result [%s] must equal -63/416", result));
    }

    //invalid input
}
