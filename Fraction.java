/* Performs mathematical operations on fractions from user input
   Author: Ryan Savino

   Referenced Source:
   https://mathcs.clarku.edu/~djoyce/cs101/Resources/Fraction.java
*/

import java.io.*;
import java.util.*;
import java.util.Arrays;

public class Fraction {
  private int numerator, denominator;

  public Fraction() {
    numerator = denominator = 0;
  }

  public Fraction(Integer _numerator, Integer _denominator) {
    numerator = _numerator;
    denominator = _denominator;
  }

  // public accessor methods for private data

  public int getNumerator() {
    return numerator;
  }

  public void setNumerator(int num) {
    numerator = num;
  }

  public int getDenominator() {
    return denominator;
  }

  public void setDenominator(int den) {
    denominator = den;
  }

  // public action methods for manipulating fractions

  public Fraction add(Fraction b) {
    if ((denominator == 0) || (b.denominator == 0))
      throw new IllegalArgumentException("Invalid denominator: undefined value");

    int common = lcd(denominator, b.denominator);

    // convert fractions to lcd
    Fraction commonA = new Fraction();
    Fraction commonB = new Fraction();
    commonA = convert(common);
    commonB = b.convert(common);

    // create new fraction to return as sum
    Fraction sum = new Fraction();

    // calculate sum
    sum.numerator = commonA.numerator + commonB.numerator;
    sum.denominator = common;

    // reduce the resulting fraction
    sum = sum.reduce();
    return sum;
  }

  public Fraction subtract(Fraction b) {
    if ((denominator == 0) || (b.denominator == 0))
      throw new IllegalArgumentException("Invalid denominator: undefined value");

    int common = lcd(denominator, b.denominator);

    // convert fractions to lcd
    Fraction commonA = new Fraction();
    Fraction commonB = new Fraction();
    commonA = convert(common);
    commonB = b.convert(common);

    // create new fraction to return as difference
    Fraction diff = new Fraction();

    // calculate difference
    diff.numerator = commonA.numerator - commonB.numerator;
    diff.denominator = common;

    // reduce the resulting fraction
    diff = diff.reduce();
    return diff;
  }

  public Fraction multiply(Fraction b) {
    if ((denominator == 0) || (b.denominator == 0))
      throw new IllegalArgumentException("Invalid denominator: undefined value");

    Fraction product = new Fraction();

    // calculate product
    product.numerator = numerator * b.numerator;
    product.denominator = denominator * b.denominator;

    // reduce the resulting fraction
    product = product.reduce();
    return product;
  }

  public Fraction divide(Fraction b) {
    if ((denominator == 0) || (b.numerator == 0))
      throw new IllegalArgumentException("Invalid denominator: undefined value");

    Fraction result = new Fraction();

    // calculate result
    result.numerator = numerator * b.denominator;
    result.denominator = denominator * b.numerator;

    // reduce the resulting fraction
    result = result.reduce();
    return result;
  }

  public String toString() {
    String buffer = numerator + "/" + denominator;
    return buffer;
  }

  // private methods used internally by Fraction class

  private int lcd(int denom1, int denom2) {
    int factor = denom1;
    while ((denom1 % denom2) != 0)
      denom1 += factor;
    return denom1;
  }

  private int gcd(int denom1, int denom2) {
    int factor = denom2;
    while (denom2 != 0) {
      factor = denom2;
      denom2 = denom1 % denom2;
      denom1 = factor;
    }
    return denom1;
  }

  private Fraction convert(int common) {
    Fraction result = new Fraction();
    int factor = common / denominator;
    result.numerator = numerator * factor;
    result.denominator = common;
    return result;
  }

  private Fraction reduce() {
    Fraction result = new Fraction();
    int common = 0;

    // get absolute values for numerator and denominator
    int num = Math.abs(numerator);
    int den = Math.abs(denominator);

    // figure out which is less, numerator or denominator
    if (num > den)
      common = gcd(num, den);
    else if (num < den)
      common = gcd(den, num);
    else
      common = num; // if both are the same, don't need to call gcd

    // set result based on common factor derived from gcd
    result.numerator = numerator / common;
    result.denominator = denominator / common;
    return result;
  }

  // converts a mixed number operand to an improper fraction
  private static Fraction convertOperandToFraction(String operand) {
    Fraction f;

    if (!operand.matches("[0-9-_/]*"))
        throw new IllegalArgumentException("Operands can only contain numbers and underscores");
        
    if (operand.contains("_")) {  // mixed number
      String[] operandArray = operand.split("_");
      
      if (operandArray.length != 2)
        throw new IllegalArgumentException("Invalid operand format. Valid example: 3_5/6");
        
      Fraction wholeNumberPart = new Fraction(Math.abs(Integer.parseInt(operandArray[0])), 1);
      Fraction fractionPart = convertOperandToFraction(operandArray[1]);
      
      // numerator in a mixed number cannot be negative; must be assigned to whole number
      if (fractionPart.numerator < 0)
        throw new IllegalArgumentException("Invalid operand format. A mixed number cannot have a negative value in the numerator. The negative value must be assigned to the whole number part.");

      // denominator in a mixed number cannot be negative; must be assigned to whole number
      if (fractionPart.denominator < 0)
        throw new IllegalArgumentException("Invalid operand format. A mixed number cannot have a negative value in the denominator. The negative value must be assigned to the whole number part.");
      
      f = wholeNumberPart.add(fractionPart);

      // set value to appropriate negative value if applicable
      if (Integer.parseInt(operandArray[0]) < 0)
        f.numerator = - f.numerator;
    } else if (operand.contains("/")) {  // no whole number part
      String[] operandArray = operand.split("/");

      if (operandArray.length != 2)
        throw new IllegalArgumentException("Invalid operand format. Valid example: 5/6");

      f = new Fraction(Integer.parseInt(operandArray[0]), Integer.parseInt(operandArray[1]));

      
    } else {  // whole number only
      f = new Fraction(Integer.parseInt(operand), 1);
    }
    
    return f;
  }

  // performs all operations for a specific operator in the linked list of operands/operators
  private static LinkedList<String> completeAllOperationsForOperator(LinkedList<String> opsList, String operator) {
    if (opsList.contains(operator)) {
      Integer operatorIndex = opsList.indexOf(operator);
      Integer operand1Index = operatorIndex - 1;
      Integer operand2Index = operatorIndex + 1;

      // ensure proper user input format
      if (operand1Index < 0 || operand2Index >= opsList.size())
        throw new IllegalArgumentException("Invalid input: operators must have a preceding and succeeding operand");
      
      String operand1 = opsList.get(operand1Index);
      Fraction f1 = convertOperandToFraction(operand1);

      String operand2 = opsList.get(operand2Index);
      Fraction f2 = convertOperandToFraction(operand2);

      Fraction calculatedFraction;

      switch (operator) {
        case "*":
          calculatedFraction = f1.multiply(f2);
          break;
        case "/":
          calculatedFraction = f1.divide(f2);
          break;
        case "+":
          calculatedFraction = f1.add(f2);
          break;
        case "-":
          calculatedFraction = f1.subtract(f2);
          break;
        default:
          throw new IllegalArgumentException("Invalid operator");
      }

      // update string list with new calculated value and remove operands and operators already calculated
      opsList.set(operand1Index, calculatedFraction.toString());
      opsList.remove(operand2Index.intValue());
      opsList.remove(operatorIndex.intValue());
    }

    // repeat until all operations are handled
    if (opsList.contains(operator))
      opsList = completeAllOperationsForOperator(opsList, operator);

    return opsList;
  }

  private static String convertFractionToMixedNumber(Fraction f) {
    if (f.numerator % f.denominator == 0) {
      return Integer.toString(f.numerator / f.denominator);
    } else if (Math.abs(f.numerator) < Math.abs(f.denominator)) {
      if (f.denominator < 0) {
        f.setDenominator(Math.abs(f.denominator));
        f.setNumerator(f.numerator * (-1));
      }
      return String.format("%d/%d", f.numerator, f.denominator);
    } else {
      Integer wholeNumber = f.numerator / f.denominator;
      Integer remainder = f.numerator % f.denominator;
      return String.format("%d_%d/%d", wholeNumber, Math.abs(remainder), Math.abs(f.denominator));
    }
  }

  private static String convertFractionToMixedNumber(String stringF) {
    if (!stringF.contains("/")) // whole number without a fraction part
      return stringF;

    if (stringF.contains("_")) { // mixed number already - recalculate
      stringF = convertOperandToFraction(stringF).toString();
    }

    String[] fractionArray = stringF.split("/");

    if (fractionArray.length != 2)
      throw new IllegalArgumentException("Invalid fraction format");
    
    Fraction f = new Fraction(Integer.parseInt(fractionArray[0]), Integer.parseInt(fractionArray[1]));
    return convertFractionToMixedNumber(f);
  }

  // calculates the equation result
  public static String calculate(String equation) {
    // equation cannot be null/empty
    if (equation == null || equation.isEmpty())
      throw new IllegalArgumentException("Equation value cannot be empty");

    // equation must be numbers, underscores and operators only (no parentheses)
    if (!equation.matches("[\\s0-9*/+-_]*"))
      throw new IllegalArgumentException("Equation can only contain numbers, underscores and operators");

    // check that operators are surrounded by white space (To Be Completed)

    // trim and split on whitespace
    LinkedList<String> opsList = new LinkedList<>(Arrays.asList(equation.trim().split("\\s+")));

    // calculate in correct order of operations
    opsList = completeAllOperationsForOperator(opsList, "*"); // multiply
    opsList = completeAllOperationsForOperator(opsList, "/"); // divide
    opsList = completeAllOperationsForOperator(opsList, "+"); // add
    opsList = completeAllOperationsForOperator(opsList, "-"); // subtract

    if (opsList.size() != 1)
      throw new IllegalArgumentException("Error calculating result: check operator placement");
    
    return convertFractionToMixedNumber(opsList.get(0));
  }
  
  public static void main(String args[]) {
    String equation = null;

    try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
      System.out.print("Please enter an equation with fractions: ");
      equation = br.readLine();
    } catch (IOException ioe) {
      System.out.println(String.format("Invalid value: %s", ioe.getMessage()));
    }

    try {
      System.out.println(calculate(equation));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
